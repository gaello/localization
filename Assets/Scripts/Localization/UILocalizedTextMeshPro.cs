﻿using UnityEngine;
using TMPro;

/// <summary>
/// Component used to update content of TextMeshPro component with translation
/// </summary>
[RequireComponent(typeof(TextMeshProUGUI))]
public class UILocalizedTextMeshPro : UILocalizedBase
{
    // Reference to TextMeshPro label component
    private TextMeshProUGUI label;

    /// <summary>
    /// Awake this instance.
    /// </summary>
    private void Awake()
    {
        label = GetComponent<TextMeshProUGUI>();
    }

    /// <summary>
    /// Receiveds the translation.
    /// </summary>
    /// <param name="text">Translated text.</param>
    protected override void ReceivedTranslation(string text)
    {
        label.text = text;
    }
}
