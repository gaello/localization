﻿using UnityEngine;

/// <summary>
/// Base class for localized labels in UI.
/// To use translations you need to create child class and override ReceivedTranslation() method.
/// </summary>
public class UILocalizedBase : MonoBehaviour
{
    // Translation key used to get translation from LocalizationManager.
    [SerializeField]
    private string labelTextKey;

    /// <summary>
    /// Override existing key for label.
    /// </summary>
    /// <param name="key">Translation key.</param>
    public void SetKey(string key)
    {
        labelTextKey = key;
        UpdateLabel();
    }

    /// <summary>
    /// Gets translation from LocalizationManager and passes it to ReceivedTranslation() method.
    /// </summary>
    private void UpdateLabel()
    {
        var translation = LocalizationManager.Instance.GetTranslation(labelTextKey);
        ReceivedTranslation(translation);
    }

    /// <summary>
    /// Receiveds the translation.
    /// Override this function to update desired label.
    /// </summary>
    /// <param name="text">Translated text.</param>
    protected virtual void ReceivedTranslation(string text)
    {
        // content of this function will be filled in child classes
    }

    /// <summary>
    /// Unity method called when component or game object is enabled.
    /// </summary>
    private void OnEnable()
    {
        UpdateLabel();

        // Attach to Language Change event
        LocalizationManager.Instance.OnLanguageChange += UpdateLabel;
    }

    /// <summary>
    /// Unity method called when component or game object is disabled.
    /// </summary>
    private void OnDisable()
    {
        // Dettach from Language Change event
        LocalizationManager.Instance.OnLanguageChange -= UpdateLabel;
    }
}
