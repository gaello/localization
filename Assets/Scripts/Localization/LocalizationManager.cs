﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Localization Manager
/// This class is responsible for loading file with translations and responding to language change.
/// It stores translations is currently loaded language.
/// </summary>
public class LocalizationManager : PersistentLazySingleton<LocalizationManager>
{
    // Path to translation file inside Resource folder
    private const string TRANSLATION_FILE_PATH = "translations";

    // Current language
    private string currentLang = "en";

    // Available languages in translation file
    private List<string> availableLanguages = new List<string>();
    // Loaded translations, key - tranlation
    private Dictionary<string, string> translations = new Dictionary<string, string>();

    // Attach to be notified on language change.
    public UnityAction OnLanguageChange;

    /// <summary>
    /// Unity method called like constructor.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();

        // Loading tranlsations
        LoadTranslations();
    }

    /// <summary>
    /// Loading translation file and changing language to default one.
    /// </summary>
    private void LoadTranslations()
    {
        ChangeLanguage(currentLang);
    }

    /// <summary>
    /// Method that changes language in game.
    /// </summary>
    /// <param name="lang">Desired language expresed in 2 letter code.</param>
    public void ChangeLanguage(string lang)
    {
        Debug.LogFormat("[{0}] Changing language to: {1}", typeof(LocalizationManager), lang);

        // Changing current game language.
        currentLang = lang;

        // Loading language.
        LoadLanguageFile(lang);

        // This line might return different language than passed in function.
        // It will happen only if desired language was not found in file.
        Debug.LogFormat("[{0}] Language changed to: {1}", typeof(LocalizationManager), currentLang);

        // Notify on language change
        OnLanguageChange?.Invoke();
    }

    /// <summary>
    /// Load file with translation and updates translation dictionary
    /// </summary>
    /// <param name="lang">Desired language.</param>
    private void LoadLanguageFile(string lang)
    {
        // Loads translation file from Resource folder
        TextAsset languageFile = Resources.Load<TextAsset>(TRANSLATION_FILE_PATH);

        // Checking if tranlation file exists
        if (!languageFile)
        {
            Debug.LogErrorFormat("There is no file in resources under path: {0}", TRANSLATION_FILE_PATH);
            return;
        }

        // Splits texts into lines for ease of use.
        var lines = languageFile.text.Split('\n');
        int langIndex = -1;

        // Clear available languages if required
        if (availableLanguages.Count > 0)
        {
            availableLanguages.Clear();
        }

        // Clear translations if required
        if (translations.Count > 0)
        {
            translations.Clear();
        }

        // Go through loaded lines
        for (int i = 0; i < lines.Length; i++)
        {
            // Remove unnecessary symbols
            var line = lines[i].Trim();

            // Split line into smaller pieces
            var part = line.Split(';');

            // First line contains language symbols
            if (i == 0)
            {
                // Loading available languages
                for (int j = 1; j < part.Length; j++)
                {
                    availableLanguages.Add(part[j]);

                    // Check which element is our desired language
                    if (part[j] == lang)
                    {
                        langIndex = j;
                    }
                }

                // In case that we don't have desired language in file we can load first (or default) one.
                if (langIndex == -1)
                {
                    langIndex = 1;
                    currentLang = part[langIndex - 1];
                }
            }
            else // In rest there are tranlsations
            {
                // Loading keys and translations for selected language.
                var key = part[0];
                translations[key] = part[langIndex];
            }
        }
    }

    /// <summary>
    /// Returns translation for provided key.
    /// </summary>
    /// <returns>Translation.</returns>
    /// <param name="key">Translation key.</param>
    public string GetTranslation(string key)
    {
        if (!translations.ContainsKey(key))
        {
            Debug.LogErrorFormat("Localization in lang: {0} is missing key: {1}", currentLang, key);
            return key;
        }

        return translations[key];
    }
}
