﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Component used to update content of Text component with translation
/// </summary>
[RequireComponent(typeof(Text))]
public class UILocalizedText : UILocalizedBase
{
    // Reference to Text label component
    private Text label;

    /// <summary>
    /// Unity method called like constructor.
    /// </summary>
    private void Awake()
    {
        label = GetComponent<Text>();
    }

    /// <summary>
    /// Receiveds the translation.
    /// </summary>
    /// <param name="text">Translated text.</param>
    protected override void ReceivedTranslation(string text)
    {
        label.text = text;
    }
}
