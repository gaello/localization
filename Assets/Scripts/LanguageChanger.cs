﻿using UnityEngine;

/// <summary>
/// Language changer show how to change language using LocalizationManager.
/// </summary>
public class LanguageChanger : MonoBehaviour
{
    /// <summary>
    /// Call method in LocalizationManager to change language.
    /// Method is called from Language Buttons
    /// </summary>
    /// <param name="lang">Desired language.</param>
    public void ChangeLanguage(string lang)
    {
        LocalizationManager.Instance.ChangeLanguage(lang);
    }
}
