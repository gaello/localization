# Localizing game in Unity

So you are looking for information about translating your game in Unity? Great! In this repository I'm giving an example how you can make an easy system that will handle translations of your game and update all labels with translation keys.

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/04/11/localizing-your-game-in-unity/

Enjoy!

---

# How to use it?

This repository contains an example of how you can implement localization in your game.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/localization/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned about localizing your game in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com
